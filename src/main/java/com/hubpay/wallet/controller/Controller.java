package com.hubpay.wallet.controller;

import com.hubpay.wallet.entity.Transaction;
import com.hubpay.wallet.entity.Wallet;
import com.hubpay.wallet.services.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/api/wallet")
public class Controller {
    private final WalletService walletService;
    @Autowired
    public Controller(WalletService walletService) {
        this.walletService = walletService;
    }

    @PostMapping("/add-funds")
    public ResponseEntity<Wallet> addFunds(@RequestParam Long userId, @RequestParam BigDecimal amount) {
        Wallet updatedWallet = walletService.addFunds(userId, amount);
        return ResponseEntity.ok(updatedWallet);
    }

    @PostMapping("/withdraw-funds")
    public ResponseEntity<Wallet> withdrawFunds(@RequestParam Long userId, @RequestParam BigDecimal amount) {
        Wallet updatedWallet = walletService.withdrawFunds(userId, amount);
        return ResponseEntity.ok(updatedWallet);
    }

    @GetMapping("/transactions")
    public ResponseEntity<Page<Transaction>> getTransactions(@RequestParam(defaultValue = "0") int page,
                                                             @RequestParam(defaultValue = "5") int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Transaction> transactions = walletService.getTransactions(pageable);
        return ResponseEntity.ok(transactions);
    }
}