package com.hubpay.wallet.entity;

public enum TransactionType {
    CREDIT, DEBIT
}
