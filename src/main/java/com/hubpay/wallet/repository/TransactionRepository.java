package com.hubpay.wallet.repository;

import com.hubpay.wallet.entity.Transaction;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

// TransactionRepository.java
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findAllByOrderByCreatedDateDesc(Pageable pageable);
}
