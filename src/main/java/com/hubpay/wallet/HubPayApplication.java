package com.hubpay.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.hubpay.wallet.repository")
public class HubPayApplication {

	public static void main(String[] args) {
		SpringApplication.run(HubPayApplication.class, args);
	}

}
