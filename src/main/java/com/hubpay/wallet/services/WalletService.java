package com.hubpay.wallet.services;

import com.hubpay.wallet.entity.Transaction;
import com.hubpay.wallet.entity.Wallet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

public interface WalletService {

    @Transactional
    Wallet addFunds(Long userId, BigDecimal amount);

    @Transactional
    Wallet withdrawFunds(Long userId, BigDecimal amount);

    Page<Transaction> getTransactions(Pageable pageable);
}