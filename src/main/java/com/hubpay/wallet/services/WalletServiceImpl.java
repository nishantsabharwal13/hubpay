package com.hubpay.wallet.services;

import com.hubpay.wallet.exceptions.EntityNotFoundException;
import com.hubpay.wallet.exceptions.InsufficientFundsException;
import com.hubpay.wallet.exceptions.InvalidAmountException;
import com.hubpay.wallet.entity.Transaction;
import com.hubpay.wallet.entity.TransactionType;
import com.hubpay.wallet.entity.Wallet;
import com.hubpay.wallet.repository.TransactionRepository;
import com.hubpay.wallet.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.math.BigDecimal;
import java.util.List;

@Service
public class WalletServiceImpl implements WalletService {

    private final WalletRepository walletRepository;
    private final TransactionRepository transactionRepository;

    private final TransactionTemplate transactionTemplate;

    @Autowired
    public WalletServiceImpl(WalletRepository walletRepository, TransactionRepository transactionRepository, TransactionTemplate transactionTemplate) {
        this.walletRepository = walletRepository;
        this.transactionRepository = transactionRepository;
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    @Transactional
    public Wallet addFunds(Long userId, BigDecimal amount) {
        validateAddAmount(amount);
        Wallet wallet = getOrCreateWallet(userId);

        BigDecimal balance = wallet.getBalance();
        wallet.setBalance(balance.add(amount));

        Transaction transaction = createTransaction(amount, TransactionType.CREDIT, wallet);
        transactionRepository.save(transaction);

        return walletRepository.save(wallet);
    }

    @Override
    @Transactional
    public Wallet withdrawFunds(Long userId, BigDecimal amount) {
        validateWithdrawAmount(amount);
        Wallet wallet = getOrCreateWallet(userId);

        return transactionTemplate.execute(status -> {
            Wallet currentWalletState = walletRepository.findById(wallet.getId())
                    .orElseThrow(() -> new EntityNotFoundException("Wallet not found"));

            BigDecimal newBalance = currentWalletState.getBalance().subtract(amount);

            if (newBalance.compareTo(BigDecimal.ZERO) < 0) {
                throw new InsufficientFundsException("Insufficient funds in the wallet");
            }

            wallet.setBalance(newBalance);

            Transaction transaction = createTransaction(amount, TransactionType.DEBIT, wallet);
            transactionRepository.save(transaction);

            return walletRepository.save(wallet);
        });
    }

    @Override
    @Transactional(readOnly = true)
    public Page<Transaction> getTransactions(Pageable pageable) {
        List<Transaction> transactions = transactionRepository.findAllByOrderByCreatedDateDesc(pageable);
        return new PageImpl<>(transactions, pageable, transactions.size());

    }

    private void validateAddAmount(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.TEN) < 0 || amount.compareTo(BigDecimal.valueOf(10000)) > 0) {
            throw new InvalidAmountException("Invalid Amount. Minimum 10 Euros Maximum 10,000 Euros");
        }
    }
    private void validateWithdrawAmount(BigDecimal amount)  {
        if (amount.compareTo(BigDecimal.valueOf(5000)) > 0) {
            throw new InvalidAmountException("Invalid Amount. Maximum 5,000 Euros");
        }
    }

    private Wallet getOrCreateWallet(Long userId) {
        return walletRepository.findByUserId(userId).orElseThrow();
    }

    private Transaction createTransaction(BigDecimal amount, TransactionType type, Wallet wallet) {
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setType(type);
        transaction.setWallet(wallet);
        return transaction;
    }
}